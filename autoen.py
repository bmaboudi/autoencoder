import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import data_handler as data
import os.path
import matplotlib.pyplot as plt

features = 16

class LinearVAE(nn.Module):
	def __init__(self):
		super(LinearVAE, self).__init__()

		self.enc1 = nn.Linear(10000,5000)
		self.enc2 = nn.Linear(5000,2500)
		self.enc3 = nn.Linear(2500,1250)
		self.enc4 = nn.Linear(1250,512)
		self.enc5 = nn.Linear(512,256)
		self.enc6 = nn.Linear(256,128)
		self.enc7 = nn.Linear(128,64)
		self.enc8 = nn.Linear(64,2*features)

		self.dec1 = nn.Linear(features,64)
		self.dec2 = nn.Linear(64,128)
		self.dec3 = nn.Linear(128,256)
		self.dec4 = nn.Linear(256,512)
		self.dec5 = nn.Linear(512,1250)
		self.dec6 = nn.Linear(1250,2500)
		self.dec7 = nn.Linear(2500,5000)
		self.dec8 = nn.Linear(5000,10000)

	def reparameterize(self, mu, log_var):
		std = torch.exp(0.5*log_var)
		eps = torch.randn_like(std)
		sample = mu + (eps * std)
		return sample

	def forward(self, x):
		 x = F.relu(self.enc1(x))
		 x = F.relu(self.enc2(x))
		 x = F.relu(self.enc3(x))
		 x = F.relu(self.enc4(x))
		 x = F.relu(self.enc5(x))
		 x = F.relu(self.enc6(x))
		 x = F.relu(self.enc7(x))
		 x = self.enc8(x).view(-1, 2, features)

		 mu = x[:, 0, :]
		 log_var = x[:, 1, :]

		 z = self.reparameterize(mu, log_var)

		 x = F.relu(self.dec1(z))
		 x = F.relu(self.dec2(x))
		 x = F.relu(self.dec3(x))
		 x = F.relu(self.dec4(x))
		 x = F.relu(self.dec5(x))
		 x = F.relu(self.dec6(x))
		 x = F.relu(self.dec7(x))
		 x = torch.sigmoid(self.dec8(x))

		 return x, mu, log_var

class TestVAE(nn.Module):
	def __init__(self):
		super(TestVAE, self).__init__()

		self.enc1 = nn.Linear(10000,5000)
		self.enc2 = nn.Linear(5000,2500)
		self.enc3 = nn.Linear(2500,1250)
		self.enc4 = nn.Linear(1250,512)
		self.enc5 = nn.Linear(512,256)
		self.enc6 = nn.Linear(256,128)
		self.enc7 = nn.Linear(128,64)
		self.enc8 = nn.Linear(64,2*features)

		self.dec1 = nn.Linear(features,64)
		self.dec2 = nn.Linear(64,128)
		self.dec3 = nn.Linear(128,256)
		self.dec4 = nn.Linear(256,512)
		self.dec5 = nn.Linear(512,1250)
		self.dec6 = nn.Linear(1250,2500)
		self.dec7 = nn.Linear(2500,5000)
		self.dec8 = nn.Linear(5000,10000)

	def reparameterize(self, mu, log_var):
		std = torch.exp(0.5*log_var)
		eps = torch.randn_like(std)
		sample = mu + (eps * std)
		return sample

	def forward(self):
		 mu = torch.ones([1,features])
		 log_var = torch.ones([1,features])
		 z = self.reparameterize(mu, log_var)

		 x = F.relu(self.dec1(z))
		 x = F.relu(self.dec2(x))
		 x = F.relu(self.dec3(x))
		 x = F.relu(self.dec4(x))
		 x = F.relu(self.dec5(x))
		 x = F.relu(self.dec6(x))
		 x = F.relu(self.dec7(x))
		 x = torch.sigmoid(self.dec8(x))

		 return x

class Final_Loss(nn.Module):
	def __init__(self):
		super().__init__()

	def forward(self, bce_loss, mu, log_var):
		BCE = bce_loss
		KLD = -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
		return BCE + KLD


def train_network():
	path = './data_train.npz'
	train_data = data.data_holder(batch_size=1024)
	train_data.read_from_file(path)

	path = './data_test.npz'
	test_data = data.data_holder(batch_size=200)
	test_data.read_from_file(path)

	num_batches = train_data.num_batches
	batch_size = train_data.batch_size
	in_dim = train_data.data_dim_in
	out_dim = train_data.data_dim_out

	use_cuda = torch.cuda.is_available()
	device_name = 'cuda' if use_cuda else 'cpu'
	device = torch.device(device_name)

	model = LinearVAE().to(device)
	optimizer = optim.Adam(model.parameters(),lr=0.001)
	scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,mode='min',factor=0.9,patience=10)
	criterion = nn.BCELoss(reduction='sum').to(device)
	loss_net = Final_Loss()

	in_device_data = train_data.torch_out[0].to(device)
	if( os.path.isfile('model.pt') == True ):
		checkpoint = torch.load('model.pt')
		model.load_state_dict(checkpoint['model_state_dict'])
		optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

	num_epocs = 1000
	for epoc in range(num_epocs):
		model.train()
		total_loss = 0.0
		for b in range(num_batches):
			in_device_data = train_data.torch_out[b].to(device)
			optimizer.zero_grad()
			out, mu, log_var = model(in_device_data)

			bce_loss = criterion(out, in_device_data)
			loss = loss_net(bce_loss, mu, log_var)
			
			total_loss += loss.item()
			loss.backward()
			optimizer.step()
		train_data.to_torch()

		mean_loss = total_loss/num_batches/batch_size
		torch.save({'epoch': epoc,'model_state_dict': model.state_dict(),'optimizer_state_dict': optimizer.state_dict(),'loss': mean_loss}, 'model.pt')

		model.eval()
		in_device_data = test_data.torch_out[0].to(device)
		out, mu, log_var = model(in_device_data)

		bce_loss = criterion(out, in_device_data)
		loss = loss_net(bce_loss, mu, log_var)

		lr = 0
		for param_group in optimizer.param_groups:
			lr = param_group['lr']
		print("Epoch %05d loss: %.10f, lr: %.7f" % (epoc + 1, loss.item(), lr ) )

def test_net():
	path = './data_test.npz'
	test_data = data.data_holder(batch_size=200)
	test_data.read_from_file(path)

	model = LinearVAE()
	checkpoint = torch.load('model.pt',map_location='cpu')
	model.load_state_dict(checkpoint['model_state_dict'])

	model.eval()
	in_data = test_data.torch_out[0]
	out, mu, log_var = model(in_data)

	out_np = out.detach().numpy()
	data_np = in_data.detach().numpy()

	for i in range(100):
		plt.subplot(121)
		plt.imshow( out_np[i].reshape([100,100]) )
		plt.colorbar()

		plt.subplot(122)
		plt.imshow( data_np[i].reshape([100,100]) )
		plt.colorbar()
		plt.show()

def test_net_generation():
	path = './data_test.npz'
	test_data = data.data_holder(batch_size=200)
	test_data.read_from_file(path)

	model = TestVAE()
	checkpoint = torch.load('model.pt',map_location='cpu')
	model.load_state_dict(checkpoint['model_state_dict'])

	model.eval()
	out = model().detach().numpy()
	plt.imshow( out.reshape([100,100]) )
	plt.show()







if __name__ == '__main__':
	#train_network()
	test_net()
	#test_net_generation()

import numpy as np
import torch

class data_holder():
	def __init__(self, batch_size=100):
		self.data_in = []
		self.data_out = []
		self.torch_in = []
		self.torch_out = []

		self.batch_size = batch_size

	def read_from_file(self, path):
		raw_data = np.load(path)
		self.data_in = raw_data['data_in']
		self.data_out = raw_data['data_out']

		self.num_params = self.data_in.shape[0]
		self.data_dim_in = self.data_in.shape[1]
		self.data_dim_out = self.data_out.shape[1]
		#self.data_dim_out_combined = self.data_dim_out[0]*self.data_dim_out[1]

		self.num_batches = int( self.num_params/self.batch_size )

		self.data_out = np.reshape(self.data_out,[self.num_params,-1])

		#print(self.data_in.shape)
		#print(self.data_out.shape)

		self.to_torch()

	def to_torch(self):
		self.torch_in.clear()
		self.torch_out.clear()

		l = np.random.permutation(self.num_params)
		for i in range(self.num_batches):
			self.torch_in.append( torch.from_numpy( self.data_in[i*self.batch_size:(i+1)*self.batch_size] ).float() )
			self.torch_out.append( torch.from_numpy( self.data_out[i*self.batch_size:(i+1)*self.batch_size] ).float() )

if __name__ == '__main__':
	data = data_holder(batch_size=200)
	data.read_from_file('./data_test.npz')

	print(data.batch_size)
	print(len(data.torch_in) )
	print(len(data.torch_out))
	print(data.torch_in[0].shape)
	print(data.torch_out[0].shape)